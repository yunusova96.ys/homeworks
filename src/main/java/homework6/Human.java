package homework6;

import homework6.Pet;

import java.util.Arrays;
import java.util.Objects;

public class Human {
    public String name;
    public String surname;
    public int dateOfBirth;
    public int IQLevel;
    public Pet pet;
    public Human mother;
    public Human father;
    public Schedule[] schedule;

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Human(String name, String surname, int dateOfBirth, int IQLevel, Pet pet, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.IQLevel = IQLevel;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = new Schedule[7];
    }

    public Human() {

    }

    public void greetPet(Pet name) {
        System.out.println("Hello," + name);
    }

    public void describePet(String species,int age) {
        System.out.printf("I have %s,he is %d years old,he is %s", species, age,age>50?"very sly":"almost not sly");
    }


    public void addSchedule(Schedule[] schedule){
        this.schedule = schedule;
    }
    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", IQLevel=" + IQLevel +
                ", pet=" + pet +
                ", mother=" + mother +
                ", father=" + father +
                ", schedule=" + Arrays.toString(schedule) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return dateOfBirth == human.dateOfBirth &&
                IQLevel == human.IQLevel &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(pet, human.pet) &&
                Objects.equals(mother, human.mother) &&
                Objects.equals(father, human.father) &&
                Arrays.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, dateOfBirth, IQLevel, pet, mother, father);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("finalize from Human class");
    }
}
