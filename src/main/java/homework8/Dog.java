package homework8;

import homework8.Pet;
import homework8.Species;

import java.util.Set;

public class Dog extends Pet {
    public Dog(String nickName, int age, int trickLevel, Set habits) {
        super(nickName, age, trickLevel, habits);
    }

    public Dog(String nickName) {
        super(nickName);
        setSpecies(Species.DOG);
    }

    @Override
    public  void respond() {
        System.out.println("havvhav");
    }

    @Override
    public void foul() {
        System.out.println("haw haw..I need to cover up");
    }
}
