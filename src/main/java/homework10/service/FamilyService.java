package homework10.service;

import homework10.dao.FamilyDao;
import homework10.entity.Human;
import homework10.entity.Pet;
import homework10.entity.Family;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

public class FamilyService {
    final FamilyDao<Family> familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public void createNewFamily(Human mother, Human father) throws Exception {
        familyDao.createNewFamily(mother, father);

    }

    public Collection<Family> getAllFamilies() throws Exception {
        return familyDao.getAll();
    }

    public void displayAllFamilies() throws Exception {

        getAllFamilies().stream().forEach(new Consumer<Family>() {
            @Override
            public void accept(Family family) {
                System.out.print(family.show());

            }
        });

    }

    public Optional<Family> getFamilyByIndex(int index) throws Exception {
        return familyDao.get(index);
    }

    public Optional<Family> getGreaterThan()
    {
        Family selectedFamily = familyDao.get(0).get();
        for (Family family : familyDao.getAll()) {
            if (family.getChildren().size() > selectedFamily.getChildren().size()) {
                selectedFamily = family;
            }

        }
        return Optional.of(selectedFamily);
    }

    public Optional<Family> getLessThan() {
        Family selectedFamily = familyDao.getAll().get(0);
        for (Family family : familyDao.getAll()) {
            if (family.getChildren().size() < selectedFamily.getChildren().size()) {
                selectedFamily = family;
            }

        }
        return Optional.of(selectedFamily);
    }

    public boolean deleteFamily(int index) throws Exception {
        return familyDao.delete(index);
    }

    private boolean addChild(Family family, Human human) {
        return family.getChildren().add(human);
    }

    public void bornChild(Family family, String masculine, String feminine) {
        Human human = createHuman(family, masculine != null ? masculine : feminine);
        addChild(family, human);
    }

    public void adoptChild(Family family, Human child) {
        addChild(family, child);
    }

    private Human createHuman(Family family, String name) {
        return new Human(name, family.getFather().getSurname(),
                LocalDate.parse("2018-03-20").toEpochDay(), 34, null, family.getMother(), family.getFather());

    }

    public int count() {
        return familyDao.getAll().size();

    }

    public Set<Pet> getPets(int index) throws Exception {
        return getFamilyByIndex(index).get().getPet();
    }

    public boolean addPets(int familyIndex, Pet pet) throws Exception {
        Optional<Family> family = familyDao.get(familyIndex);
        if (family.isPresent()){
            family.get().getPet().add(pet);
            return  true;
        }
        else  {
            return false;
        }
    }

    public boolean deleteOlderThan(int index) {
        Family selectedFamily = familyDao.getAll().get(0);
        for (Family family : familyDao.getAll()) {
            family.getChildren().removeIf(child -> LocalDate.now().getYear()-child.getDateOfBirth().getYear()>index);

        }
        return false;
    }
}
