package homework2;

import java.util.Random;
import java.util.Scanner;

public class Game {
    private Scanner scanner;
    int[][] gameGrid = new int[6][6];
    private int targetColumn;
    private int targetRow;

    public Game() {
        this.scanner = new Scanner(System.in);
    }

    private void printGrid(){
        for (int row = 0;row<6;row++){
            for(int column = 0; column<6; column++){
                if(row==0){
                    System.out.printf("%d | ",column);
                }else{
                    if(column==0){
                        System.out.printf("%d | ",row);
                    }else{
                        System.out.printf("%s | ",gameGrid[row][column]==0?"-":(gameGrid[row][column]==1?"*":"x"));
                    }
                }

            }
            System.out.println();
        }
    }


    private boolean checkInput(int input){
        return input>0&&input<6;
    }
    private int getInput(){
        boolean isLineTrue = false;
        int input = 0;
        while (!isLineTrue) {
            System.out.println("Please enter a line for fire between 1-5 ");
            System.out.print("line:");
            input = scanner.nextInt();
            if (checkInput(input)) {
                isLineTrue = true;
            } else {
                System.out.println("Opss...Wrong number type,please try again");
            }
        }
        return input;
    }


    /**
     * if row column equals to target it will return true;
     * @param inputColumn
     * @param inputRow
     * @return
     */
    private boolean checkAfterInputs(int inputColumn, int inputRow){
        return inputColumn==targetColumn&&inputRow==targetRow;
    }

    private void getTargetPoint(){
        targetRow = new Random().nextInt(5)+1;
        targetColumn = new Random().nextInt(5)+1;

    }
    public void startGame(){
        boolean gameStatus = true;
        int inputRow = 0;
        int inputColumn = 0;
        getTargetPoint();

        printGrid();

        while(gameStatus){
            inputRow = getInput();
            inputColumn = getInput();
            if(checkAfterInputs(inputColumn,inputRow)){
                gameGrid[inputRow][inputColumn]=2;
                System.out.println("Congrats,finally you won :)");
                gameStatus = false;
            }else{
                gameGrid[inputRow][inputColumn]=1;
            }

            printGrid();
        }
    }
}
