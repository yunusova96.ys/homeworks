package homework13;

import homework13.IO.MenuConsole;

public class MainAppp {

    public static void main(String[] args) {
        LogUtility.addLog("Main App", "starting", LogUtility.LogTypes.INFO);

        new MenuConsole().startApp();

    }

}
