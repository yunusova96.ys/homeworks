package homework4;

public class FamilyApp {
    public static void main(String[] args) {
        String[] habits = {"eat", "sleep", "drink"};
        Pet dog = new Pet("dog", "Rock", 2, 75, habits);

        Human mother = new Human("Jane Karelone");
        Human father = new Human("Vito Karelone");
        Human human = new Human("Michail", "Karleone", 1977, 90, dog, mother,father);


        System.out.println(dog);
        System.out.println(human);



    }
}
