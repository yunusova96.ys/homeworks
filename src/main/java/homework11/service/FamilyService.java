package homework11.service;

import homework11.dao.FamilyDao;
import homework11.entity.Family;
import homework11.entity.Human;
import homework11.entity.Pet;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FamilyService {
    final FamilyDao<Family> familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public void createNewFamily(Human mother, Human father) throws Exception {
        familyDao.createNewFamily(mother, father);

    }

    public Collection<Family> getAllFamilies() throws Exception {
        return familyDao.getAll();
    }

    public void displayAllFamilies() throws Exception {

        getAllFamilies().stream().forEach(new Consumer<Family>() {
            @Override
            public void accept(Family family) {
                System.out.print(family.show());

            }
        });

    }

    public Optional<Family> getFamilyByIndex(int index) throws Exception {
        return familyDao.get(index);
    }

    public Optional<List<Family>> getBiggerThan(int numb) {
        return Optional.of(familyDao.getAll().stream().filter(family -> family.getChildren().size() + 2 > numb).collect(Collectors.toList()));


    }

    public Optional<List<Family>> getLessThan(int numb) {
        return Optional.of(familyDao.getAll().stream().filter(family -> family.getChildren().size() + 2 < numb).collect(Collectors.toList()));

    }

    public boolean deleteFamily(int index) throws Exception {
        return familyDao.delete(index);
    }

    private boolean addChild(Family family, Human human) {
        return family.getChildren().add(human);
    }

    public void bornChild(Family family, String masculine, String feminine) {
        Human human = createHuman(family, masculine != null ? masculine : feminine);
        addChild(family, human);
    }

    public void adoptChild(Family family, Human child) {
        addChild(family, child);
    }

    private Human createHuman(Family family, String name) {
        return new Human(name, family.getFather().getSurname(),
                1996, 34, null, family.getMother(), family.getFather());

    }

    public int countFamiliesWithMemberNumber(int number) {
        List<Family> familyList = familyDao.getAll().stream().filter(family ->
                (family.getChildren().size() + 2) == number)
                .collect(Collectors.toList());
        int size = familyList.size();

        return size;


    }

    public Set<Pet> getPets(int index) throws Exception {
        return getFamilyByIndex(index).get().getPet();
    }

    public boolean addPets(int familyIndex, Pet pet) throws Exception {
        Optional<Family> family = familyDao.get(familyIndex);
        if (family.isPresent()) {
            family.get().getPet().add(pet);
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteOlderThan(int index) {
        for (Family family : familyDao.getAll()) {
            family.getChildren().removeIf(child -> LocalDate.now().getYear() - child.getYear() > index);

        }
        return false;
    }
}
