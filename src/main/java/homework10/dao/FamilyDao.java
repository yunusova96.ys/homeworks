package homework10.dao;

import homework10.entity.Human;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * this DAO doesn't cover any connection problems
 */
public interface FamilyDao<A> {
  Optional<A> get(int id);
  List<A> getAll();
  List<A> getAllBy(Predicate<A> p);
  void createNewFamily(Human mother, Human father);
  boolean delete(int id);
// Optional<A> getFamiliesBiggerThan();
// boolean deleteOlderThan(int index);
// Optional<A> getFamiliesLessThan();
// boolean addChild(A family, Human child);
// boolean addPet(int index, Pet pet);
// int count();
}
