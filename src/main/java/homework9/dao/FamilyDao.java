package homework9.dao;

import homework9.entity.Family;
import homework9.entity.Human;
import homework9.entity.Pet;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * this DAO doesn't cover any connection problems
 */
public interface FamilyDao<A> {
  Optional<A> get(int id);
  List<A> getAll();
  List<A> getAllBy(Predicate<A> p);
  void createNewFamily(Human mother, Human father );
  boolean delete(int id);

}
