package homework8;

import homework7.Pet;
import homework7.Species;

public class RoboCat extends Pet {
    public RoboCat(String nickName) {
        super(nickName);
        setSpecies(Species.ROBOCAT);
    }

    @Override
    public void respond() {
        System.out.println("Hello my owner, I am your Robocat");
    }
}
