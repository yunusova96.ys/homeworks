package homework8;

import homework8.Species;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public abstract class Pet {
    private  Species species=Species.UNKNOWN;//cinsi
    private  String nickName;
    private  int age;
    private  int trickLevel;//whole numb 1-100
    private Set<String> habits;

    public Pet() {
    }

    public Pet(String nickName) {
        this.nickName = nickName;
        habits=new HashSet();
    }

      public Pet(String nickName, int age, int trickLevel, Set<String> habits) {
        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }


    public  void eat(){
        System.out.print("Im eating");
    }

    public abstract void  respond();
    public  void foul(){
        System.out.println("I need to cover it up!");
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species + '\'' +
                ", nickName='" + nickName + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }
}
