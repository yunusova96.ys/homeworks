package homework12.dao;

import homework12.entity.Human;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * this DAO doesn't cover any connection problems
 */
public interface FamilyDao<A> {
  Optional<A> get(int id);
  List<A> getAll();
  List<A> getAllBy(Predicate<A> p);
  void createNewFamily(Human mother, Human father);
  boolean delete(int id);

}
