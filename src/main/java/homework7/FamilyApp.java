package homework7;

import homework6.Human;
import homework6.Species;
import homework6.*;

public class FamilyApp {
    public static void main(String[] args) {

        Dog dog= new Dog("bob");
        dog.eat();
        System.out.println();
        dog.foul();
        System.out.println();
        dog.respond();
        System.out.println();
        Fish fish = new Fish("nemo");
        fish.eat();


        Man man = new Man("Jim","Johnatan",1962,70,dog,null,null);
        Woman woman = new Woman("Janny","Johnatan",1962,80,fish,null,null);
        Family family = new Family(man,woman);




    }
}
