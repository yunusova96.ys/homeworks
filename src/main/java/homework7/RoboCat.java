package homework7;

public class RoboCat extends Pet {
    public RoboCat(String nickName) {
        super(nickName);
        setSpecies(Species.ROBOCAT);
    }

    @Override
    public void respond() {
        System.out.println("Hello my owner, I am your Robocat");
    }
}
