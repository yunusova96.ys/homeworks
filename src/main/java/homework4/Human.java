package homework4;

import java.time.LocalDate;
import java.util.Arrays;

public class Human {
    public String name;
    public String surname;
    public int dateOfBirth;
    public int IQLevel;
    public Pet pet;
    public Human mother;
    public Human father;
    public String[][] schedule;



    public Human(String name, String surname, int dateOfBirth, int IQLevel, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.IQLevel = IQLevel;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }
    public Human(String name, String surname, int dateOfBirth, int IQLevel, Pet pet, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.IQLevel = IQLevel;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
    }


    public Human(String s) {
    }


    public void greetPet(Pet name) {
        System.out.println("Hello," + name);
    }

    public void describePet(String species,int age) {
        System.out.printf("I have %s,he is %d years old,he is %s", species, age,age>50?"very sly":"almost not sly");
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", IQLevel=" + IQLevel +
                ", pet=" + pet +
                ", mother=" + mother +
                ", father=" + father +
                ", schedule=" + Arrays.toString(schedule) +
                '}';
    }
}
