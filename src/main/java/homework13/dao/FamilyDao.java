package homework13.dao;

import homework13.entity.Human;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * this DAO doesn't cover any connection problems
 */
public interface FamilyDao<A> {
  Optional<A> get(int id);
  List<A> getAll();
  List<A> getAllBy(Predicate<A> p);
  void createNewFamily(Human mother, Human father);
  boolean delete(int id);

  default Collection<A> getAll(String fileName){
    try {
      File file = new File("./database",fileName);
      FileInputStream fis = new FileInputStream(file);
      ObjectInputStream ois = new ObjectInputStream(fis);
      Object obj = ois.readObject();
      ois.close();
      fis.close();
      return (ArrayList<A>) obj;
    } catch (IOException | ClassNotFoundException e) {
      create(fileName,new ArrayList<>());
//            System.out.println(e);
      return new ArrayList<>();
    }
  }

  default void create(String fileName, Collection<A> all){
    try{
//      Files.write(Paths.get(fileName),all, StandardCharsets.UTF_8);
      File file = new File("./database",fileName);
      FileOutputStream fos = new FileOutputStream(file);
      ObjectOutputStream oos = new ObjectOutputStream(fos);
      oos.writeObject(all);
      oos.close();
      fos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
