package homework8;

import homework8.Pet;
import homework8.Species;

public class Fish extends Pet {
    public Fish(String nickName) {
        super(nickName);
        setSpecies(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.println("..............");
    }


}
