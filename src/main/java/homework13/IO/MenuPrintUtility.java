package homework13.IO;

import java.util.ArrayList;
import java.util.List;

public class MenuPrintUtility {

    private static MenuPrintUtility instance;

    public static MenuPrintUtility getInstance(){
        if(instance==null){
            instance = new MenuPrintUtility();
        }
        return instance;
    }


    private List<HeaderInfo> headersList;

    public void addHeader(HeaderInfo headerInfo){
        if(headersList==null){
            init();
        }

        headersList.add(headerInfo);
    }

    public void refreshAll(){
        init();
    }
    private void init(){
        headersList = new ArrayList<>();
    }


    public void printHeaders(){
        printUPofHeader();
        System.out.println();
        printHeader();
        System.out.println();
        printDownOfHeader();
        System.out.println();




    }

    private void printHeader(){
        for (HeaderInfo headerInfo : headersList) {
            if(headersList.indexOf(headerInfo)==0){
                System.out.print("│");
            }
                System.out.print(
                        completeAndGiveData(
                                completeAndGiveData(
                                        " ",
                                        headerInfo.headerLength/2-headerInfo.header.length()
                                ).concat(headerInfo.header),headerInfo.headerLength
                        ));
            System.out.print("│");
        }
    }

    private void printUPofHeader(){
        for (HeaderInfo headerInfo : headersList) {
            if(headersList.indexOf(headerInfo)==0){
                System.out.print("┌");
            }
            for (int i = 0; i<headerInfo.headerLength;i++){
                System.out.print("─");
            }
            if(headersList.indexOf(headerInfo)<headersList.size()-1){
                System.out.print("┬");
            }else{
                System.out.print("┐");
            }
        }
    }

    public void printFooter(){
        for (HeaderInfo headerInfo : headersList) {
            if(headersList.indexOf(headerInfo)==0){
                System.out.print("└");
            }
            for (int i = 0; i<headerInfo.headerLength;i++){
                System.out.print("─");
            }
            if(headersList.indexOf(headerInfo)<headersList.size()-1){
                System.out.print("┴");
            }else{
                System.out.print("┘");
            }
        }
        System.out.println();
    }

    private void printDownOfHeader(){
        for (HeaderInfo headerInfo : headersList) {
            if(headersList.indexOf(headerInfo)==0){
                System.out.print("├");
            }
            for (int i = 0; i<headerInfo.headerLength;i++){
                System.out.print("─");
            }
            if(headersList.indexOf(headerInfo)<headersList.size()-1){
                System.out.print("┼");
            }else{
                System.out.print("┤");
            }
        }
    }

    public void printData(String... data){
        for (int i = 0;i<data.length;i++) {
            if(i==0){
                System.out.print("│");
            }
            System.out.print(
                    completeAndGiveData(
                            data[i],headersList.get(i).headerLength
                    ));


            System.out.print("│");
        }
        System.out.println();


    }


    private String completeAndGiveData(String data,int size){
        if(data.length()<size){
           while(data.length()<size){
               data = data.concat(" ");
           }
        }
        return data;
    }




    public static class HeaderInfo{
        private final String header;
        private final int headerLength;

        public HeaderInfo(String header, int headerLength) {
            this.header = header;
            this.headerLength = headerLength;
        }

        public String getHeader() {
            return header;
        }

        public int getHeaderLength() {
            return headerLength;
        }
    }
}
