package homework8;

import homework8.Human;
import homework8.Pet;

import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List children;
    private Set<Pet> pet;
    int childcount = 0;


    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        children= new ArrayList();

    }


    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List getChildren() {
        return children;
    }

    public void setChildren(List children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pet);
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pet +
                '}';
    }

    public boolean addChild(Human child) {
        children.add(child);

        return true;
    }


    public boolean deleteChild(Human child) {
        if (!children.isEmpty()) {
           children.remove(child);
            return true;
        }

        return false;
    }

    public  int getCountFamily(){
        int fc=0;
        fc = 2+ children.size();
        return  fc;
    }
}
