package homework12.IO;

import com.sun.scenario.effect.impl.sw.java.JSWBlend_SRC_OUTPeer;
import homework12.controller.FamilyController;
import homework12.customException.FamilyOverflowException;
import homework12.utility.MenuPrintUtility;

import java.util.Scanner;

public class MenuConsole {
    private final Scanner scanner;
    private final FamilyController familyController;


    public MenuConsole() {
        this.scanner = new Scanner(System.in);
        familyController = new FamilyController(scanner);
    }

    public void startApp() {
        int n = -1;
        while (n != 0) {

                n = getNumber(getMainMenu());


            switch (n) {
                case 1: {
                    familyController.fillTestData();

                    break;
                }
                case 2: {
                    familyController.displayAll();
                    break;
                }
                case 3: {
                    menu3();
                    break;
                }
                case 4: {
                    menu4();
                    break;
                }
                case 5: {
                    menu5();
                    break;
                }
                case 6: {
                    menu6();
                    break;
                }
                case 7: {
                    menu7();
                    break;
                }
                case 8: {
                    menu8();
                    break;
                }
                case 9: {
                    menu9();
                    break;
                }
                case 0: {
                    System.out.println("Bye bye");
                    break;
                }
                default:
                    wrongMenuSelection();
            }
        }
    }


    private void menu3() {
        String result = null;
        try {
            result = familyController.getGreaterThan(getNumber("please enter number for greater than\nNumber : "));
        } catch (Exception e) {
            System.out.println("Be careful with your choice");
        }
        if (result != null) {
            System.out.println(result);
        } else {
            System.out.println("Not found");
        }
    }

    private void menu4() {
        String result = null;
        try {
            result = familyController.getLessThan(getNumber("please enter number for less than\nNumber : "));
        } catch (Exception e) {
            System.out.println("Be careful with your choice");

        }
        if (result != null) {
            System.out.println(result);
        } else {
            System.out.println("Not found");
        }
    }

    private void menu5() {
        int result = 0;
        try {
            result = familyController.countFamiliesWithMemberNumber(getNumber("please enter number for count member\nNumber : "));
        } catch (Exception e) {
            System.out.println("Be careful with your choice");

        }
        if (result != 0) {
            System.out.println(result);
        } else {
            System.out.println("Not found");
        }
    }

    private void menu6() {
        familyController.createNewFamily();
    }

    private void menu7() {
        familyController.deleteFamily();
    }

    private void menu8() {
        int m = 0;
        while (m != 3) {
            try {
                m = getNumber(getMenu8());
            } catch (Exception e) {
                System.out.println("Be careful with your choice");
                break;

            }
            switch (m) {
                case 1: {
                    try {
                    familyController.bornChild();

                    }catch (FamilyOverflowException e){
                        System.out.println(e.getMessage());
                    }
                    break;
                }
                case 2: {
                    try {
                        familyController.adoptCild();

                    }catch (FamilyOverflowException e){
                        System.out.println(e.getMessage());
                    }
                    break;
                }
                case 3: {
                    break;
                }
                default:
                    wrongMenuSelection();
            }
        }
    }

    private void menu9() {
        familyController.deleteOlderThan();
    }

    private void seperate() {
        System.out.println("************************************");
    }

    private void wrongMenuSelection() {
        System.out.println("wrong menu selected");
    }

    private int getNumber(String menu)  {
        try {
            System.out.print(menu);
            return scanner.nextInt();

        }catch (Exception e){
            System.out.println("You have to enter integer");
            scanner.nextLine();
        return -1;
        }
    }

    private String getString(String menu) {
        System.out.print(menu);
        return scanner.next();
    }



    private String getMenu8() {
        menuInfo8();
        return "->";
    }

    private String getMainMenu() {
         showMenuInfo();
         return "->";
    }

    public void showMenuInfo() {
        MenuPrintUtility instance = MenuPrintUtility.getInstance();
        instance.addHeader(new MenuPrintUtility.HeaderInfo("No ", 6));
        instance.addHeader(new MenuPrintUtility.HeaderInfo("Family App", 60));
        MenuPrintUtility.getInstance().printHeaders();
        MenuPrintUtility.getInstance().printData("1.", "Fill with test data");
        MenuPrintUtility.getInstance().printData("2.", "Display All list of families");
        MenuPrintUtility.getInstance().printData("3.", "Display families greater than");
        MenuPrintUtility.getInstance().printData("4.", "Display families less than");
        MenuPrintUtility.getInstance().printData("5.", "Calculate families");
        MenuPrintUtility.getInstance().printData("6.", "Create a new family");
        MenuPrintUtility.getInstance().printData("7.", "Delete a family by index");
        MenuPrintUtility.getInstance().printData("8.", "Edit a family by its index");
        MenuPrintUtility.getInstance().printData("9.", "Remove children over the age");
        MenuPrintUtility.getInstance().printFooter();

        MenuPrintUtility.getInstance().refreshAll();
    }
    public void menuInfo8() {
        MenuPrintUtility instance = MenuPrintUtility.getInstance();
        instance.addHeader(new MenuPrintUtility.HeaderInfo("No ", 6));
        instance.addHeader(new MenuPrintUtility.HeaderInfo("Edit a family by its index", 60));
        MenuPrintUtility.getInstance().printHeaders();
        MenuPrintUtility.getInstance().printData("1.", "Give birth to a baby");
        MenuPrintUtility.getInstance().printData("2.", "Adopt a child");
        MenuPrintUtility.getInstance().printData("3.", "Return to main menu ");
        MenuPrintUtility.getInstance().printFooter();

        MenuPrintUtility.getInstance().refreshAll();
    }
}
