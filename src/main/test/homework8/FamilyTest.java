package homework8;

import homework8.Family;
import homework8.Human;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    Family family;

    @BeforeEach
    void setUp() {
        Human father = new Human("Eli veliyev");
        Human mother = new Human("Gulnar veliyeva");
        Human child1 = new Human("Baby veliyev");
        family = new Family(mother, father);
        family.addChild(child1);
    }


    @Test
    void getMother() {

        Human motherTest = new Human("Marilyn Monro");
        assertFalse(motherTest.equals(family.getMother()));
    }
    @Test
    void getFather() {

        Human fatherTest = new Human("John Snow");
        assertFalse(fatherTest.equals(family.getFather()));
    }


    @Test
    void addChild() {
        Human child2 = new Human("Jone veliyev");
        assertTrue(family.addChild(child2));

    }

    @Test
    void deleteChild() {
        Human child2 = new Human("Jone veliyev");
        family.addChild(child2);
        assertTrue(family.deleteChild(child2));

    }

    @Test
    void getCountFamily() {
        int size = family.getChildren().size();
        assertEquals(2 + size, family.getCountFamily());

    }

    @Test
    void testToString() {
        String unExpected = "Testing our to string";
        System.out.println(family.toString());
        assertNotEquals(unExpected, family.toString());
    }

    @Test
    void testEquals() {
        Human mother = new Human("Gulnise Pashayeva");
        Human father = new Human("Tofiq Pashayev");
        Family familyTest = new Family(mother, father);
        assertFalse(this.family.equals(familyTest));
    }

    @Test
    void testHashCode() {
        assertNotEquals(family.hashCode(), 0);

    }


    @Test
    void getPet() {
        Pet dog = new Pet("Rocky") {
            @Override
            public void respond() {

            }
        };

        assertFalse(dog.equals(family.getPet()));
    }

}