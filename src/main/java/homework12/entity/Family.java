package homework12.entity;


import com.sun.corba.se.spi.ior.Identifiable;
import org.omg.CORBA_2_3.portable.OutputStream;

import java.io.Serializable;
import java.util.*;

public class Family implements Serializable {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pet;
    int childcount = 0;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        children = new ArrayList<>();
        pet = new HashSet<>();
    }

    public Family(Human mother, Human father, List<Human> children, Set<Pet> pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return childcount == family.childcount &&
                Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pet);
    }

    @Override
    public String toString() {
        return "Family{" +"\n"+
                "mother=" + mother +"\n"+
                "father=" + father +"\n"+
                "children=" + children +"\n"+
                "pet=" + pet +
                '}';
    }

    public String prettyFormat() {
        String format = String.format("Mother\t\t:\t%s\nFather\t\t:\t'%s'\nChildren : \n", mother, father);
        for (Human child : children) {
            format = format
                    .concat("\t\t")
                    .concat(child.getGender().getGender())
                    .concat("\t:\t")
                    .concat(child.toString()).concat("\n");

        }
        format = format.concat("Pets\t\t:\t").concat(pet.toString()).concat("\n");
        return format;
    }


    public int getId() {
        return 0;
    }


    public void write(OutputStream arg0) {

    }
}
