package homework4;

import java.util.Arrays;

public class Pet {
    private  String species;//cinsi
    private  String nickName;
    private  int age;
    private  int trickLevel;//whole numb 1-100
    private String[] habits;

    public Pet() {
    }

    public Pet(String nickName) {
        this.species = nickName;
    }

      public Pet(String species, String nickName, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickName = nickName;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }


    public  void eat(){
        System.out.print("Im eating");
    }

    public  void  respond(String nickName){
        System.out.print("Hello, owner. I am "+nickName+" I miss you!");
    }

    public  void foul(){
        System.out.println("I need to cover it up!");
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species + '\'' +
                ", nickName='" + nickName + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }
}
