package homework13.entity;

public enum Gender {
    MASCULINE("Boy"),FEMININE("Girl");

    private  String gender;


    Gender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }
}
