package homework12.service;

import homework12.dao.CollectionFamilyDao;
import homework12.entity.Family;
import homework12.entity.Human;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    Family family;
    FamilyService familyService;


    @BeforeEach
    void setUp() {
        Human father = new Human("Eli", "veliyev");
        Human mother = new Human("Gulnar", "veliyeva");
        Human child1 = new Human("Baby", "veliyev");
        family = new Family(mother, father);
        familyService=new FamilyService(new CollectionFamilyDao());
        familyService.adoptChild(family,child1);

    }

    @Test
    void createNewFamily() throws Exception {
        Human father = new Human("Eli", "veliyev");
        Human mother = new Human("Gulnar", "veliyeva");
        int expectedSize = familyService.getAllFamilies().size();
        familyService.createNewFamily(mother,father);
        assertEquals(expectedSize+1,familyService.getAllFamilies().size());

    }

    @Test
    void getAllFamilies() throws Exception {
        Human father = new Human("Eli", "veliyev");
        Human mother = new Human("Gulnar", "veliyeva");
        familyService.createNewFamily(mother,father);
        assertNotEquals(0,familyService.getAllFamilies().size());

    }

    @Test
    void getFamilyByIndex() throws Exception {
        Human father = new Human("Eli", "veliyev");
        Human mother = new Human("Gulnar", "veliyeva");
        familyService.createNewFamily(mother,father);
        Optional<Family> familyByIndex = familyService.getFamilyByIndex(0);
        equals(!familyByIndex.isPresent());

    }

    @Test
    void getBiggerThan() throws Exception {
        Human father = new Human("Eli", "veliyev");
        Human mother = new Human("Gulnar", "veliyeva");
        Human child2 = new Human("Baby", "veliyev");
        familyService.createNewFamily(mother,father);
        Family familyByIndex = familyService.getFamilyByIndex(0).get();
        familyService.bornChild(familyByIndex,null,"Gulnar");
        List<Family> families = familyService.getBiggerThan(1).get();
        assertNotEquals(0,families.size());
    }

    @Test
    void getLessThan() throws Exception {
        Human father = new Human("Eli", "veliyev");
        Human mother = new Human("Gulnar", "veliyeva");
        Human child2 = new Human("Baby", "veliyev");

        familyService.createNewFamily(mother,father);
        Family familyByIndex = familyService.getFamilyByIndex(0).get();
        familyService.bornChild(familyByIndex,null,"Gulnar");
        List<Family> families = familyService.getLessThan(1).get();
        assertEquals(0,families.size());
    }

    @Test
    void deleteFamily() throws Exception {
        Human father = new Human("Eli", "veliyev");
        Human mother = new Human("Gulnar", "veliyeva");
        Human child2 = new Human("Baby", "veliyev");

        familyService.createNewFamily(mother,father);
        int before = familyService.getAllFamilies().size();
        familyService.deleteFamily(0);
        int after = familyService.getAllFamilies().size();
        assertNotEquals(before,after);


    }

    @Test
    void bornChild() throws Exception {
        Human father = new Human("Eli", "veliyev");
        Human mother = new Human("Gulnar", "veliyeva");
        Human child2 = new Human("Baby", "veliyev");

        familyService.createNewFamily(mother,father);
        Family familyByIndex = familyService.getFamilyByIndex(0).get();
        int before = familyByIndex.getChildren().size();
        familyService.bornChild(familyByIndex,null,"Gulnar");
        int after = familyByIndex.getChildren().size();
        assertNotEquals(before,after);
    }

    @Test
    void adoptChild() throws Exception {
        Human father = new Human("Eli", "veliyev");
        Human mother = new Human("Gulnar", "veliyeva");
        Human child2 = new Human("Baby", "veliyev");

        familyService.createNewFamily(mother,father);
        Family familyByIndex = familyService.getFamilyByIndex(0).get();
        int before = familyByIndex.getChildren().size();
        familyService.adoptChild(familyByIndex,child2);
        int after = familyByIndex.getChildren().size();
        assertNotEquals(before,after);
    }


}