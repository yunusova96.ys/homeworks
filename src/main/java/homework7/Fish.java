package homework7;

public class Fish extends Pet {
    public Fish(String nickName) {
        super(nickName);
        setSpecies(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.println("..............");
    }


}
