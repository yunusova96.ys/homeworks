package homework13.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    Family family;

    @BeforeEach
    void setUp() {
        Human father = new Human("Eli"," veliyev");
        Human mother = new Human("Gulnar"," veliyeva");
        family = new Family(mother, father);
    }


    @Test
    void getMother() {

        Human motherTest = new Human("Marilyn"," Monro");
        assertFalse(motherTest.equals(family.getMother()));
    }
    @Test
    void getFather() {

        Human fatherTest = new Human("John"," Snow");
        assertFalse(fatherTest.equals(family.getFather()));
    }



    @Test
    void testToString() {
        String unExpected = "Testing our to string";
        System.out.println(family.toString());
        assertNotEquals(unExpected, family.toString());
    }

    @Test
    void testEquals() {
        Human mother = new Human("Gulnise"," Pashayeva");
        Human father = new Human("Tofiq"," Pashayev");
        Family familyTest = new Family(mother, father);
        assertFalse(this.family.equals(familyTest));
    }

    @Test
    void testHashCode() {
        assertNotEquals(family.hashCode(), 0);

    }


    @Test
    void getPet() {
        Pet dog = new Pet("Rocky") {
            @Override
            public void respond() {

            }
        };

        assertFalse(dog.equals(family.getPet()));
    }

}