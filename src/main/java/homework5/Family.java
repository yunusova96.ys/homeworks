package homework5;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    int childcount = 0;



    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }


    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(children, family.children) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pet);
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

    public boolean addChild(Human child) {
       if(children==null){
           children = new Human[1];
           children[0] = child;
       }else{
            addNextChild(child);
       }

        return true;
    }


    private void addNextChild(Human child){
        children = Arrays.copyOf(children,children.length+1);
        children[children.length-1] = child;
    }

    public boolean deleteChild(int childIndex) {
        if(children==null){
            return false;
        }
        if(children.length>1){
            return deleteChildFromChildren(childIndex);
        }else{
            children = null;
            return  true;
        }
    }


    private boolean deleteChildFromChildren(int childIndex){
        Human[] newArrayOfChildren = new Human[children.length-1];
        int j = 0;
        for(int i = 0; i<children.length;i++){
            if(i!=childIndex){
                newArrayOfChildren[j] = children[i];
                j++;
            }
        }
        children = newArrayOfChildren;
        return true;
    }
    public  int getCountFamily(){
        return 2+ children.length;
    }
}
