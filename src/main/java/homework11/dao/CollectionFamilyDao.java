package homework11.dao;

import homework11.entity.Family;
import homework11.entity.Human;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao<Family> {

    private static CollectionFamilyDao instance;
    private List<Family> listOfFamily;

    public static CollectionFamilyDao getInstance() {
        if (instance == null) {
            instance = new CollectionFamilyDao();
        }
        return instance;
    }

    public CollectionFamilyDao() {
        listOfFamily = new ArrayList<>();
    }

    @Override
    public Optional<Family> get(int index) {
        return Optional.of(listOfFamily.get(index));
    }

    @Override
    public List<Family> getAll() {
        return listOfFamily;
    }

    @Override
    public List<Family> getAllBy(Predicate<Family> p) {
        return getAll().stream().filter(p).collect(Collectors.toList());
    }

    @Override
    public void createNewFamily(Human mother, Human father) {
        listOfFamily.add(new Family(mother, father));


    }

    @Override
    public boolean delete(int id) {
        return delete(getAll().stream().filter(family -> family.getId() == id).findFirst().get());

    }


    public boolean delete(Family family) {
        if (family != null) {
            return listOfFamily.remove(family);
        } else {
            return false;
        }
    }

}
