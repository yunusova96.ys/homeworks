package homework13.dao;

import homework13.LogUtility;
import homework13.entity.Family;
import homework13.entity.Human;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao<Family> {

    private static CollectionFamilyDao instance;
    private final static  String fileName="families.txt";

    public static CollectionFamilyDao getInstance() {
        if (instance == null) {
            instance = new CollectionFamilyDao();
        }
        return instance;
    }

    public CollectionFamilyDao() {
    }

    @Override
    public Optional<Family> get(int index) {
        LogUtility.addLog("CollectionFamilyDao", "getting family by id", LogUtility.LogTypes.INFO);

        return Optional.of(getAll().get(index));
    }


    @Override
    public List<Family> getAll()
    {
        LogUtility.addLog("CollectionFamilyDao", "getting all", LogUtility.LogTypes.INFO);

        return (List<Family>) getAll(fileName);
    }

    @Override
    public List<Family> getAllBy(Predicate<Family> p) {
        LogUtility.addLog("CollectionFamilyDao", "getting all by", LogUtility.LogTypes.INFO);
        return getAll().stream().filter(p).collect(Collectors.toList());
    }

    @Override
    public void createNewFamily( Human mother, Human father) {
        LogUtility.addLog("CollectionFamilyDao", "creatingNewFamily", LogUtility.LogTypes.INFO);
        List<Family> all = getAll();
        all.add(new Family(mother, father));
        create(fileName,all);


    }

    @Override
    public boolean delete(int id) {
        LogUtility.addLog("CollectionFamilyDao", "deleting  by id...", LogUtility.LogTypes.INFO);

        return delete(getAll().stream().filter(family -> family.getId() == id).findFirst().get());

    }


    public boolean delete(Family family) {
        LogUtility.addLog("CollectionFamilyDao", "deleting by family", LogUtility.LogTypes.INFO);

        if (family != null) {
            List<Family> all = getAll();
             all.remove(family);
             create(fileName,all);
             return true;
        } else {
            return false;
        }
    }

}
