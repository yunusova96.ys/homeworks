package homework12.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
    Human human;

    @BeforeEach
    void setUp() {
         human = new Human("John"," Snow");
    }
    public void greetPet(Pet name) {
        System.out.println("Hello," + name);
    }
    @Test
    void greetPet() {
       Pet pet= new Pet("CAT") {
           @Override
           public void respond() {

           }
       };
       String expected = "Hello,CAT";
       greetPet(pet);
    }

    @Test
    void testToString() {
        assertNotEquals("I fed up rom this test stufff", human.toString());

    }

    @Test
    void testEquals() {
        Human humanTest = new Human("Arya"," Stark");
        assertFalse(human.equals(humanTest));
    }

    @Test
    void testHashCode() {
        int unExpected = 98765;
        assertNotEquals(unExpected, human.hashCode());
    }

}