package homework10;

import homework9.controller.FamilyController;
import homework9.entity.Human;
import homework9.entity.Pet;

public class MainAppp {

    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();

        familyController.createNewFamily(
                new Human("Lucy", " gsf"),
                new Human("John ", "snow")
        );
        familyController.createNewFamily(
                new Human("NISE", " gsf"),
                new Human("aBULFAZ ", "snow")
        );
        familyController.createNewFamily(
                new Human("Pakize", " gsf"),
                new Human("Akif ", "snow")
        );

        System.out.println("getting All Family:\n" + familyController.getAllFamilies());
        System.out.println("*********************************************\n*********************************************");

        System.out.println("getting family by index..\n" + familyController.getFamilyByIndex(1));

        System.out.println("adding pet to the family with given index\n"
                + familyController.addPets(1, new Pet("nemo")));
        System.out.println("family count:" + familyController.getCount());
        System.out.println("pet:" + familyController.getPets(1));
        System.out.println("deleting family by index:" + familyController.deleteFamily(0));
        System.out.println("family count:" + familyController.getCount());
        familyController.bornChild(familyController.getFamilyByIndex(0).get(),"Tofiq","Gulbadam");
        familyController.adoptCild(familyController.getFamilyByIndex(0).get(),new Human("Baby","ssdv"));
        familyController.getFamilyByIndex(0).get().getChildren().get(0).setYear(1998);
        familyController.getFamilyByIndex(0).get().getChildren().get(1).setYear(2013);
        System.out.println(familyController.getFamilyByIndex(0).get().getChildren());
        familyController.deleteOlderThan(10);
        System.out.println(familyController.getFamilyByIndex(0).get().getChildren());
    }
}
