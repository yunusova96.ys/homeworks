package homework10.entity;

import homework4.Pet;

import java.time.LocalDate;
import java.time.Period;
import java.util.Map;

public class Human {
    private String name;
    private String surname;
    private long dateOfBirth;
    private int IQLevel;
    private Pet pet;
    private Human mother;
    private Human father;
    private Map schedule;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getDateOfBirth() {
        return LocalDate.ofEpochDay(dateOfBirth);
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getIQLevel() {
        return IQLevel;
    }

    public void setIQLevel(int IQLevel) {
        this.IQLevel = IQLevel;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Map getSchedule() {
        return schedule;
    }

    public void setSchedule(Map schedule) {
        this.schedule = schedule;
    }

    public Human(String name, String surname, long dateOfBirth, int IQLevel, Pet pet, Human mother, Human father, Map schedule) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.IQLevel = IQLevel;
        this.pet = pet;
        this.mother = mother;
        this.father = father;

        this.schedule = schedule;
    }
 public Human(String name, String surname, long dateOfBirth, int IQLevel) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.IQLevel = IQLevel;

    }


    public String describeAge() {

        return "year:\t" + calculateAge(LocalDate.ofEpochDay(dateOfBirth), LocalDate.now()) + "\nmonth:\t"
                + calculateMonth(LocalDate.ofEpochDay(dateOfBirth), LocalDate.now()) + "\nday:\t"
                + calculateDay(LocalDate.ofEpochDay(dateOfBirth), LocalDate.now());

    }

    private int calculateAge(LocalDate birthDate, LocalDate currentDate) {
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }

    private int calculateMonth(LocalDate birthDate, LocalDate currentDate) {
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getMonths();
        } else {
            return 0;
        }
    }

    private int calculateDay(LocalDate birthDate, LocalDate currentDate) {
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getDays();
        } else {
            return 0;
        }
    }


    public Human(String name, String surname, long dateOfBirth, int IQLevel, Pet pet, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.IQLevel = IQLevel;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }


    public Human() {
    }


    public void greetPet(Pet name) {
        System.out.println("Hello," + name);
    }

    public void describePet(String species,int age) {
        System.out.printf("I have %s,he is %d years old,he is %s", species, age,age>50?"very sly":"almost not sly");
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", dateOfBirth=" + LocalDate.ofEpochDay(dateOfBirth)+
                ", IQLevel=" + IQLevel +
                ", pet=" + pet +
                ", mother=" + mother +
                ", father=" + father +
                ", schedule=" + schedule+
                '}';
    }
}
