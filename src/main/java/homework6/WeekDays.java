package homework6;

public enum WeekDays {
    SUNDAY("Sunday"),
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday");

    private String dayOfWeek;

    WeekDays (String dayOfWeek){
        this.dayOfWeek = dayOfWeek;
    }

    public String getDayOfWeek(){
        return dayOfWeek;
    }

}
