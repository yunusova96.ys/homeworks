package homework6;

public class Schedule {
    private String weekDays;
    private  String task;

    public Schedule(String weekDays, String task) {
        this.weekDays = weekDays;
        this.task = task;
    }

    public String getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(String weekDays) {
        this.weekDays = weekDays;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "weekDays='" + weekDays + '\'' +
                ", task='" + task + '\'' +
                '}';
    }
}
