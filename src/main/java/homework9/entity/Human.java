package homework9.entity;

import homework4.Pet;

import java.util.Map;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int IQLevel;
    private Pet pet;
    private Human mother;
    private Human father;
    private Map schedule;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIQLevel() {
        return IQLevel;
    }

    public void setIQLevel(int IQLevel) {
        this.IQLevel = IQLevel;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Map getSchedule() {
        return schedule;
    }

    public void setSchedule(Map schedule) {
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year, int IQLevel, Pet pet, Human mother, Human father, Map schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.IQLevel = IQLevel;
        this.pet = pet;
        this.mother = mother;
        this.father = father;

        this.schedule = schedule;
    }



    public Human(String name, String surname, int year, int IQLevel, Pet pet, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.IQLevel = IQLevel;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }


    public Human() {
    }


    public void greetPet(Pet name) {
        System.out.println("Hello," + name);
    }

    public void describePet(String species,int age) {
        System.out.printf("I have %s,he is %d years old,he is %s", species, age,age>50?"very sly":"almost not sly");
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", dateOfBirth=" + year +
                ", IQLevel=" + IQLevel +
                ", pet=" + pet +
                ", mother=" + mother +
                ", father=" + father +
                ", schedule=" + schedule+
                '}';
    }
}
