package homework8;


import java.util.Map;

public class Woman extends Human {
    public Woman(String name, String surname, int dateOfBirth, int IQLevel, Pet pet, Human mother, Human father, Map schedule) {
        super(name, surname, dateOfBirth, IQLevel, pet, mother, father, schedule);
    }

    public Woman(String name, String surname, int dateOfBirth, int IQLevel, Pet pet, Human mother, Human father) {
        super(name, surname, dateOfBirth, IQLevel, pet, mother, father);
    }

    public Woman(String s) {
        super(s);
    }

    @Override
    public void greetPet(Pet name) {
        System.out.println("Hello my baby");
    }
    public void makeUp(){
        System.out.println("I am doing my awesome makeup");
    }
}
