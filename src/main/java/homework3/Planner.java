package homework3;

import java.util.Scanner;

public class Planner {

    private Scanner scanner;

    public Planner() {
        this.scanner = new Scanner(System.in);
    }

    public String[][] createArrayOfWeek() {

        String[][] schedule = new String[7][2];
        schedule[0][0] = "SUNDAY";
        schedule[0][1] = "do home work";
        schedule[1][0] = "MONDAY";
        schedule[1][1] = "go to work";
        schedule[2][0] = "TUESDAY";
        schedule[2][1] = "go to IbaTech academy";
        schedule[3][0] = "WEDNESDAY";
        schedule[3][1] = "go to work,do coding";
        schedule[4][0] = "THURSDAY";
        schedule[4][1] = "go to ibatech academy";
        schedule[5][0] = "FRIDAY";
        schedule[5][1] = "go to cinema))";
        schedule[6][0] = "SATURDAY";
        schedule[6][1] = "go to course";
        return schedule;

    }

    private boolean checkInput(String input) {
        String modifiedInput = input.toUpperCase().trim();
        if (modifiedInput.contains("SUNDAY") ||
                modifiedInput.contains("MONDAY") ||
                modifiedInput.contains("TUESDAY") ||
                modifiedInput.contains("WEDNESDAY") ||
                modifiedInput.contains("THURSDAY") ||
                modifiedInput.contains("FRIDAY") ||
                modifiedInput.contains("EXIT") ||
                modifiedInput.contains("SATURDAY")) {
            return true;
        } else {
            return false;
        }
    }

    public String getInput() {
        String weekDay = null;
        boolean isWeekday = false;
        while (!isWeekday) {
            System.out.println("Please, input the day of the week:");
            System.out.print("week:");
            weekDay = scanner.nextLine();
            if (checkInput(weekDay)) {
                isWeekday = true;
            } else {
                System.out.println("Sorry, I don't understand you, please try again.");
            }
        }


        return weekDay;
    }

    public String start() {

        createArrayOfWeek();
        String input = getInput();
        checkDayOfTask(input);
        return input;
    }

    private  void  printMessage(String day,String task){
        System.out.println("************************\n***********************\n");
        System.out.println("Your task for "+day.toLowerCase()+" is "+task);
    }

    private  void  checkDayOfTask(String input){

            String[][] weekArray = createArrayOfWeek();
            for (int i = 0; i < 7; i++) {
                int j = 0;
                if (input.equalsIgnoreCase(weekArray[i][j])) {

                    printMessage(input, weekArray[i][j + 1]);
                    break;
                }
            }



    }

}

