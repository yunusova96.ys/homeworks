package homework11.controller;

import homework11.dao.CollectionFamilyDao;
import homework11.entity.Family;
import homework11.entity.Human;
import homework11.entity.Pet;
import homework11.service.FamilyService;

import java.util.*;

public class FamilyController {
    FamilyService familyService;

    public FamilyController() {
        familyService = new FamilyService(new CollectionFamilyDao());
    }

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }
        public  void  createNewFamily(Human mother, Human father){
            try {
                familyService.createNewFamily(mother,father);
            } catch (Exception e) {
                System.out.println("error creting new family:"+e);
            }
        }

    public Collection<Family> getAllFamilies() {
        try {
            return familyService.getAllFamilies();

        } catch (Exception e) {
            System.out.println("empty database...");

            return new ArrayList<>();
        }

    }

    public void displayAll() {
        try {
            familyService.displayAllFamilies();
        } catch (Exception e) {
            System.out.println("No family found to display in database..");
        }


    }

    public Optional<Family> getFamilyByIndex(int index) {
        try {
            return familyService.getFamilyByIndex(index);
        } catch (Exception e) {
            System.out.println("no family has found with given index...");
            return Optional.empty();
        }
    }

    public boolean deleteFamily(int index) {
        try {
            return familyService.deleteFamily(index);
        } catch (Exception e) {
            System.out.println("no  family found to delete");
            return  false;
        }
    }

    public void bornChild(Family family, String masculine, String feminine) {
        familyService.bornChild(family, masculine,feminine);

    }
  public void adoptCild(Family family, Human child) {
        familyService.adoptChild(family, child);

    }

    public int getCount() {
        try {
            return familyService.getAllFamilies().size();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Set<Pet> getPets(int index) {
        try {
            return familyService.getPets(index);
        } catch (Exception e) {
            System.out.println("no pets has found with given index...");
            return null;
        }
    }

    public boolean addPets(int familyIndex, Pet pet) {
        try {
            return familyService.addPets(familyIndex, pet);
        } catch (Exception e) {
            System.out.println("no family has found to add pets with given index...");
            return false;
        }
    }


    public boolean deleteOlderThan(int index) {
        return familyService.deleteOlderThan(index);
    }

    public  int countFamiliesWithMemberNumber(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number to now size\n number:");
        int input = scanner.nextInt();
        return familyService.countFamiliesWithMemberNumber(input);
    }

}
