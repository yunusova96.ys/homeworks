package homework8;

import homework7.Pet;
import homework7.Species;

public class DomesticCat extends  Pet{
    public DomesticCat(String nickName) {
        super(nickName);
        setSpecies(Species.CAT);
    }

    @Override
    public void foul() {
        System.out.println("I need to go out to cover up meow..");
    }

    @Override
    public void respond() {
        System.out.println("Meow");
    }
}
