package homework7;

import homework5.Human;
import homework5.Pet;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    int childcount = 0;


    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;

    }

    public Family(Man man, Woman woman) {


    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pet);
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pet +
                '}';
    }

    public boolean addChild(Human child) {
        children[childcount++] = child;

        return true;
    }


    public boolean deleteChild(Human child) {
        if (childcount >= 0) {
            children[childcount--] = null;
            return true;
        }

        return false;
    }

    public  int getCountFamily(){
        int fc=0;
        fc = 2+ children.length;
        return fc;
    }
}
