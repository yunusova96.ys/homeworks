package homework6;

public class FamilyApp {
    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            Human human= new Human();
            System.gc();
        }

        Schedule[] schedules = new Schedule[7];
        schedules[0] = new Schedule(WeekDays.SUNDAY.getDayOfWeek(),"Watch film");
        schedules[1] = new Schedule(WeekDays.MONDAY.getDayOfWeek(),"Watch film again");
        schedules[2] = new Schedule(WeekDays.TUESDAY.getDayOfWeek(),"go to course");
        schedules[3]= new Schedule(WeekDays.WEDNESDAY.getDayOfWeek(),"do step project with Shafa");
        schedules[4]= new Schedule(WeekDays.THURSDAY.getDayOfWeek(),"go to course");
        schedules[5]= new Schedule(WeekDays.FRIDAY.getDayOfWeek(),"do Iba homeworks");
        schedules[6]= new Schedule(WeekDays.SATURDAY.getDayOfWeek(),"go to Batut and enjoy!!!");


        Pet pet=new Pet(Species.CAT,"Mestan", 3,10, new String[]{"eats","meow","sleep"});
        Human mother = new Human("Pakize", "Pashayeva", 1975, 24, null, null, null);
        mother.addSchedule(schedules);
        Human father = new Human("Fazil", "Pashayev", 1965, 24, null, null, null);
        father.addSchedule(schedules);
        Human child1 = new Human("Ebulfez", "Pashayev", 1995, 24, null, mother,father);
        Human child2=new Human("Xanim","Pashayeva",1996,90,pet,mother,father);
        Family family=new Family(mother,father);
        family.addChild(child1);
        family.addChild(child2);
        System.out.println(family);
        System.out.println(family.getCountFamily());
    }
}
