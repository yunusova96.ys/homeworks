package homework5;

public class FamilyApp {
    public static void main(String[] args) {
        Pet pet = new Pet("dog","puppy",2,45,null);

        Human mother = new Human("Jenny","Karleone",1988,34,pet,null,null);
        Human father = new Human("John","Karleone",1978,24,pet,null,null);

        Human child1 = new Human("Mark","Karleone",1998,54,pet,mother,father);
        Human child2 = new Human("Kate","Karleone",1999,54,pet,mother,father);
        Human child3 = new Human("Tony","Karleone",2001,54,pet,mother,father);
        Family family = new Family(mother,father);

        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        System.out.println("after adding 3 child to the family,family is:\n"+family);
        System.out.println("family count is:"+family.getCountFamily());
        family.deleteChild(2);
        System.out.println("after deleting index 2,family is:"+family.getCountFamily());

    }
}
