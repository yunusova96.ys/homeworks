package homework13.controller;

import homework13.customException.FamilyOverflowException;
import homework13.dao.CollectionFamilyDao;
import homework13.entity.Family;
import homework13.entity.Gender;
import homework13.entity.Human;
import homework13.entity.Pet;
import homework13.service.FamilyService;

import java.time.LocalDate;
import java.util.*;

public class FamilyController {
    private Scanner scanner;
    FamilyService familyService;

    public FamilyController(Scanner scanner) {
        familyService = new FamilyService(new CollectionFamilyDao());
        this.scanner = scanner;

    }

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    private void createNewFamily(Human mother, Human father) {
        try {
            familyService.createNewFamily(mother, father);
        } catch (Exception e) {
            System.out.println("error creating new family:" + e);
        }
    }

    public Collection<Family> getAllFamilies() {
        try {
            return familyService.getAllFamilies();

        } catch (Exception e) {
            System.out.println("empty database...");

            return new ArrayList<>();
        }

    }

    public void displayAll() {
        try {
            familyService.displayAllFamilies();
        } catch (Exception e) {
            System.out.println("No family found to display in database..");
        }


    }

    public Optional<Family> getFamilyByIndex(int index) {
        try {
            return familyService.getFamilyByIndex(index);
        } catch (Exception e) {
            System.out.println("no family has found with given index...");
            return Optional.empty();
        }
    }

    public boolean deleteFamily() {
        try {

            return familyService.deleteFamily(getNumber("Enter int index to delete family\t\t:\t"));
        } catch (Exception e) {
            System.out.println("no  family found to delete");
            return false;
        }
    }

    private void bornChild(Family family, String masculine, String feminine) {
        familyService.bornChild(family, masculine, feminine);

    }

    private void adoptCild(Family family, Human child) {
        familyService.adoptChild(family, child);

    }

    public int countFamiliesWithMemberNumber(int numb) {
        return familyService.countFamiliesWithMemberNumber(numb);
    }

    public Set<Pet> getPets(int index) {
        try {
            return familyService.getPets(index);
        } catch (Exception e) {
            System.out.println("no pets has found with given index...");
            return null;
        }
    }

    public boolean addPets(int familyIndex, Pet pet) {
        try {
            return familyService.addPets(familyIndex, pet);
        } catch (Exception e) {
            System.out.println("no family has found to add pets with given index...");
            return false;
        }
    }


    public boolean deleteOlderThan() {
        return familyService.deleteOlderThan(getNumber("enter age to remove children over..age\t\t:"));
    }

    public String getGreaterThan(int number) {
        Optional<List<Family>> biggerThan = familyService.getBiggerThan(number);
        return biggerThan.map(Object::toString).orElse(null);
    }

    public String getLessThan(int number) {
        Optional<List<Family>> lessThan = familyService.getLessThan(number);
        return lessThan.map(Object::toString).orElse(null);
    }

    private Human createMomOrDad(String momOrDad) {
        System.out.printf("Please enter %s details\n", momOrDad);
        Human human = new Human();
        human.setName(getString("Name\t\t : \t"));
        human.setSurname(getString("Surname\t\t:\t"));
        human.setDateOfBirth(LocalDate.of(getNumber("Year(yyyy)\t\t:\t"),
                getNumber("Month(MM)\t\t:\t"),
                getNumber("Date(dd)\t\t:\t")).toEpochDay());
        human.setIQLevel(getNumber("IQ\t\t:\t"));
        return human;
    }

    private String getString(String menu) {
        System.out.print(menu);
        return scanner.next();
    }

    private int getNumber(String menu) {
        System.out.print(menu);
        return scanner.nextInt();
    }

    public void createNewFamily() {
        Human mother = createMomOrDad("Mom");
        Human father = createMomOrDad("Dad");
        createNewFamily(mother, father);
    }
    public void createNewFamilyTest() {
        Human father1 = new Human("John", "Snow");
        Human mother1 = new Human("Daenary", "Targerian");
        Human father2 = new Human("Tommen", "Baratheon");
        Human mother2 = new Human("Sansa", "Stark");
        createNewFamily(mother1, father1);
        createNewFamily(mother2, father2);

    }

    public void bornChild() {
        int index = getNumber("enter family identifier ID\t\t:\t");
        Optional<Family> family = getFamilyByIndex(index);
        if (family.isPresent()) {
            if (family.get().getChildren().size() == 2) {
                throw new FamilyOverflowException("Heyy..too much children.. :)");
            }
            int genderNumb = getNumber("Select Gender(Enter 1 for Boy,2 for girl)\t\t:\t");
            String name = getString("Enter name for baby\t\t:\t");
            if (genderNumb == 1) {
                bornChild(family.get(), name, null);
            } else {
                bornChild(family.get(), null, name);

            }
        }else {
            System.out.println("Couldn't find family with given ID");
        }

    }
    public void fillTestData() {

        createNewFamilyTest();

    }


    public void adoptCild() {
        int index = getNumber("enter family identifier ID\t\t:\t");
        Optional<Family> family = getFamilyByIndex(index);
        if (family.isPresent()) {
            if (family.get().getChildren().size() == 2) {
                throw new FamilyOverflowException("Heyy..too much children.. :)");
            }
            int genderNumb = getNumber("Select Gender(Enter 1 for Boy,2 for girl)\t\t:\t");

            Human human = new Human();
            human.setName(getString("Enter full name of adopted child\t\t:\t"));
            human.setDateOfBirth(LocalDate.parse(getString( "Enter date of birth of child\t\t:\t")).toEpochDay());
            human.setIQLevel(getNumber("Enter the intelligence of child\t\t:\t"));
            if(genderNumb==1){
                human.setGender(Gender.MASCULINE);
            }else {
                human.setGender(Gender.FEMININE);
            }
            adoptCild(family.get(),human);
        }else {
            System.out.println("Couldn't find family with given ID");
        }

    }


}
